splurt (0.6.13) UNRELEASED; urgency=low

  * Fix for AAC format including apostrophes

 -- Jeff McLean <jeff.mclean@exemail.com.au>  13 Feb 2023 19:25:00 +1000

splurt (0.6.12) UNRELEASED; urgency=low

  * Added AAC format

 -- Jeff McLean <jeff.mclean@exemail.com.au>  20 Dec 2022 16:40:00 +1000

splurt (0.6.11) UNRELEASED; urgency=low

  * Fixed storage sync mounting issues

 -- Jeff McLean <jeff.mclean@exemail.com.au>  05 Aug 2020 16:30:00 +1000

splurt (0.6.10) UNRELEASED; urgency=low

  * Changed emit.com to nucleusstreaming.com
  * Fixed bootstrap dependencies

 -- Jeff McLean <jeff.mclean@exemail.com.au>  05 Aug 2020 16:30:00 +1000

splurt (0.6.9) UNRELEASED; urgency=low

  * Fixed sync mechanism to work with podcasts and logs

 -- Jeff McLean <jeff.mclean@exemail.com.au>  24 Feb 2020 16:30:00 +1000

splurt (0.6.8) UNRELEASED; urgency=low

  * Updated sync mechanism with secondary storage to be controlled by splurt.conf

 -- Jeff McLean <jeff.mclean@exemail.com.au>  10 Feb 2020 16:30:00 +1000

splurt (0.6.7) UNRELEASED; urgency=low

  * fixed awk problem 

 -- Jeff McLean <jeff.mclean@exemail.com.au>  06 Oct 2019 07:00:00 +1000

splurt (0.6.6) UNRELEASED; urgency=low

  * added function for announcers to hear combined audio of their show 

 -- Jeff McLean <jeff.mclean@exemail.com.au>  29 Mar 2019 23:31:00 +1000

splurt (0.6.5) UNRELEASED; urgency=low

  * fixed concatenation with sox for combined audio, rather than mixing
  * removed unnecessary white noise file 2secwhitenoise.ogg

 -- Jeff McLean <jeff.mclean@exemail.com.au>  21 Mar 2019 16:30:00 +1000

splurt (0.6.4) UNRELEASED; urgency=low

  * fixed progs.dat dependencies
  * added file schematic diagram for documentation
  * fixed cut podcasts with new year in filepath

 -- Jeff McLean <jeff.mclean@exemail.com.au>  Thu, 24 May 2018 16:30:00 +1000

splurt (0.6.3) UNRELEASED; urgency=low

  * added sampled audio to status screen
  * added link from functions page to status screen

 -- Jeff McLean <jeff.mclean@exemail.com.au>  Thu, 17 May 2018 15:30:00 +1000

splurt (0.6.2) UNRELEASED; urgency=low

  * added auto-trigger for autocut after every HQ recording

 -- Jeff McLean <jeff.mclean@exemail.com.au>  Fri, 23 Mar 2018 11:30:00 +1000

splurt (0.6.1) UNRELEASED; urgency=low

  * added ability to provide one merged file on autocut
  * added ability to specify more than one program for log quality
  * changed over to all sox utilities, away from arecord and oggenc

 -- Jeff McLean <jeff.mclean@exemail.com.au>  Fri, 16 Mar 2018 11:30:00 +1000

splurt (0.6) UNRELEASED; urgency=low

  * introduced status page
  * updated debian control scripts with interpreter
  * added version information and disk space to status screen

 -- Jeff McLean <jeff.mclean@exemail.com.au>  Wed, 24 May 2017 15:49:00 +1000

splurt (0.5) UNRELEASED; urgency=low

  * introduced graphical check for sound
  * removed slash from program names as it created directories when saving files
  * added duration parameter in config file for controlling how long the line-in check collects audio before reporting levels

 -- Jeff McLean <jeff.mclean@exemail.com.au>  Fri, 28 Oct 2016 13:49:00 +1000

splurt (0.4) UNRELEASED; urgency=low

  * introduced config files
  * introduced autocut
  * altered the default passwords of the online services

 -- Jeff McLean <jeff.mclean@exemail.com.au>  Mon, 14 Dec 2015 16:11:00 +1000

splurt (0.3) UNRELEASED; urgency=low

  * added extra storage location function for synchronising large sets of podcasting and log files to another two stores
  * changed the way that the grid progs.dat files work, so that we now have the same format for pulse and native files, but that a symlink file is used to choose between the two
  * use local version of twitter bootstrap
  * fixed url for host as variable in the cut_podcasts location reference
  * added already cut podcasts to podcasting screen
  * retrieve default sample card rate from /proc/asound files rather than default to 48000

 -- Jeff McLean <jeff.mclean@exemail.com.au>  Mon, 14 Dec 2015 16:11:00 +1000

splurt (0.2) UNRELEASED; urgency=low

  * added functions page, summarising functions available
  * added bootstrap formatting to some pages
  * changed source control to git
  * tidied up code
  * standardised debian/* files somewhat
  * standardised html filenames and removed extraneous directories
  * removed name WYN-FM from all files and replaced with generic

 -- Jeff McLean <jeff.mclean@exemail.com.au>  Fri, 14 Aug 2015 12:27:00 +1000
