#!/usr/bin/python3
import sys, os, subprocess, cgi, cgitb
cgitb.enable()
from configobj import ConfigObj

# get command line parameters
config = ConfigObj('/etc/splurt/splurt.conf')
lineInCheckPeriod = config['lineInCheckPeriod']
recFilenameStore = config['recFilenameStore']
workDir = config['workdir']

# get information about the system
cmd = "date"
p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
time = p.stdout.read().decode()

cmd = "df -h"
p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
diskSpace = p.stdout.read().decode()

cmd = "dpkg-query -s splurt"
p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
versionInfo = p.stdout.read().decode()

cmd = "ps -ef | grep \"rec -r\" | grep -v grep"
p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
rec = p.stdout.read().decode()

cmd = "cat /etc/splurt/splurt.conf"
p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
conf = p.stdout.read().decode()

cmd = "debconf-show splurt"
p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
debconf = p.stdout.read().decode()

cmd = "cat " + workDir + recFilenameStore
p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
recFilename = p.stdout.read().decode()

# set variables for lineincheck analysis
subprocess.call("mkdir -p /tmp/splurt",shell=True)

#start running lineincheck analysis
analysisString = "rec -V -c 2 -b 16 -r 48000 /tmp/splurt/lineincheck.ogg trim 0 " + lineInCheckPeriod
subprocess.call(analysisString,shell=True)
analysisString = "sox /tmp/splurt/lineincheck.ogg /tmp/splurt/lineincheck.dat stat stats > /tmp/splurt/lineincheck.info 2>&1"
subprocess.call(analysisString,shell=True)
cmd = "cat /tmp/splurt/lineincheck.info"
p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
textLevels = p.stdout.read().decode()

# execute plot
subprocess.call('nice -n 19 gnuplot /usr/share/splurt/lineincheck.gpi',shell=True)

# write webpage
print ("Content-type:text/html")
print ("")
webPage = """
<!doctype html>
<html>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<link rel="stylesheet" href="/javascript/bootstrap/css/bootstrap.min.css">
	<script src="/javascript/jquery/jquery.min.js"></script>
	<script src="/javascript/bootstrap/js/bootstrap.min.js"></script>
	<link rel='stylesheet' type='text/css' href='/splurt/html/styles.css'>
	<title>SPLURT System Status</title>
</head>
<body>
	<div class='container'>
		<img src='/splurt/logo' alt="Radio station logo">
		<div class="row">
			<div class="col-md-4">
				<h1>Time</h1>
			</div>
			<div class="col-md-8">
				<pre>{0}</pre>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<h1>Graph and stats of line-in audio</h1>
			</div>
			<div class="col-md-8">
				<img src="/tmp/splurt/lineincheck.png" />
                <br>
                <audio controls>
                    <source src="/tmp/splurt/lineincheck.ogg" type="audio/ogg">
                </audio> 
                <br>
				<pre>{1}</pre>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<h1>Version information</h1>
			</div>
			<div class="col-md-8">
				<pre>{2}</pre>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<h1>Disk space</h1>
			</div>
			<div class="col-md-8">
				<pre>{3}</pre>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<h1>Currently recording filename</h1>
			</div>
			<div class="col-md-8">
				<pre>{4}</pre>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<h1>Recording processes</h1>
			</div>
			<div class="col-md-8">
				<pre>{5}</pre>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<h1>SPLURT configuration preferences</h1>
			</div>
			<div class="col-md-8">
				<pre>{6}</pre>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<h1>SPLURT installation answers</h1>
			</div>
			<div class="col-md-8">
				<pre>{7}</pre>
			</div>
		</div>
	</div>
</body>
</html>
""" 
print (webPage.format(time, textLevels, versionInfo, diskSpace, recFilename, rec, conf, debconf))

