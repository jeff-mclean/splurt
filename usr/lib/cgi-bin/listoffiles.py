#!/usr/bin/python3
import sys, os, subprocess, cgi, cgitb, mutagen
cgitb.enable()
from configobj import ConfigObj

# get command line parameters
config = ConfigObj('/etc/splurt/splurt.conf')

# set variables
q = cgi.FieldStorage()
podcastDir = config['pcastwork']
workingDir = config['workdir']
rows = config['maxPodcastCuts']
inputmodifier = q.getvalue("fname")
fileList = []

print ("Content-type:text/html")
print ("")
webPage = """
<!doctype html>
<html>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<link rel="stylesheet" href="/javascript/bootstrap/css/bootstrap.min.css">
	<script src="/javascript/jquery/jquery.min.js"></script>
	<script src="/javascript/bootstrap/js/bootstrap.min.js"></script>
	<link rel='stylesheet' type='text/css' href='/splurt/html/styles.css'>
	<title>Choose file and cutlist</title>
</head>
<body>
	<div class='container'>
        <img src='/splurt/logo' alt='Radio station logo'>
"""
print (webPage)

for r, d, f in os.walk(workingDir + podcastDir) :
    for item in f :
        if inputmodifier == None  or item.lower().find(inputmodifier.lower()) !=-1 :
            fileList.append(r + "/" + item)
fileList.sort()

if len(fileList) != 0 :
    webPage = """
        <h1>Which one of the following files do you want to make podcasts from:</h1>
        <form role="form" action='/cgi-bin/cutfile.py' method='POST' target='_blank'>
    """
    print (webPage)

    for fileName in fileList :
        fileInfo = mutagen.File(fileName)
        print ("<input type='radio'  name='file' value=\"{}\">{}, \
            {}</input><br>\n".format(fileName,fileInfo["title"][0],fileInfo["date"][0]))

    #print table heading
    webPage = """
    <br><h1>Please type in your edit list for the program<br></h1>
    <i>Please give times in the format 1h32.05.  This example means 1 hour, 32 minutes, 5 seconds. You could also write this as 92.05, which is 92 minutes and 5 seconds.  If you don't want to worry about seconds, don't put in the '.' So 92 would just be 92 minutes.  1h32 would be exactly the same thing.<p></i>
    <b><i><u>********<br>If you are going to specify your podcasts to the second, make sure you put in '05' rather than 5.  Your seconds MUST CONTAIN two digits, or the podcast probably won't be created.  I'd personally leave out seconds here.<br>*********<p></i></b></u>
    <i>Also, the description that you enter will be the new file name that is created, so make sure you enter a different description for each one.  Otherwise, one will overwrite the other.  Of course, if you don't provide any description, that file will not be accessible.<br><br></i>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <td>Edit number</td><td>Start</td><td>Finish</td><td>Description</td>
                </tr>
            </thead>
            <tbody>
    """
    print (webPage)

    for i in range (1,int(rows)) :
        print ("<tr>")
        sn = "start" + str(i)
        finishn = "finish" + str(i)
        descn = "desc" + str(i)
        print ('<td>{}</td><td><input type="text" name="{}"></td><td><input \
        type="text" name="{}"></td><td><input type="text" \
        name="{}"></td>'.format(i, sn, finishn, descn))
        print ("</tr>")

    #print trailer
    webPage = """
            </tbody>
        </table>
    </div>
    <button type="submit" class="btn btn-default">Cut file</button>
    </form>
    """
    print (webPage)
else :
    webPage = """
        <p>No programmes were found with "{}" in the name.  Please go back and
        try again. <p>
    """.format(inputmodifier)
    print (webPage)

#print trailer
webPage = """
</div>
</body>
</html>
"""
print (webPage)
