#!/usr/bin/python3
import sys, os, subprocess, cgi, cgitb
cgitb.enable()
from configobj import ConfigObj

# get command line parameters
config = ConfigObj('/etc/splurt/splurt.conf')
workDir = config['workdir']
progsNativeDataFile = config['progsNativeDataFile']

# set variables
q = cgi.FieldStorage()
fileContents = q.getvalue("progs")

print ("Content-type:text/html")
print ("")
webPage = """
<!doctype html>
<html>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<link rel="stylesheet" href="/javascript/bootstrap/css/bootstrap.min.css">
	<script src="/usr/javascript/jquery/jquery.min.js"></script>
	<script src="/javascript/bootstrap/js/bootstrap.min.js"></script>
	<link rel='stylesheet' type='text/css' href='/splurt/html/styles.css'>
	<title>Thanks!</title>
</head>
<body>
    <div class='container'>
	    <img src='/splurt/logo' alt='Radio station logo'>
"""
print (webPage)
if not fileContents :
    print ("There was a problem uploading your file.  Please ensure that you \
    selected the right file.")
else :
    f = open(progsNativeDataFile, "wb")
    f.write(fileContents)
    f.close()
    print ("<p><br>Thanks for uploading your file</p>")
webPage = """
    </div>
</body>
</html>
"""
print (webPage)
