#!/usr/bin/python3
import sys, os, subprocess, cgi, cgitb
cgitb.enable()
from configobj import ConfigObj

# get command line parameters
config = ConfigObj('/etc/splurt/splurt.conf')
lineInCheckPeriod = config['lineInCheckPeriod']

# set variables
subprocess.call("mkdir -p /tmp/splurt",shell=True)

# write webpage
print ("Content-type:text/html")
print ("")
webPage = """
<!doctype html>
<html>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<link rel="stylesheet" href="/javascript/bootstrap/css/bootstrap.min.css">
	<script src="/javascript/bootstrap/js/bootstrap.min.js"></script>
	<link rel='stylesheet' type='text/css' href='/splurt/html/styles.css'>
	<title>Line-in check</title>
</head>
<body>
    <div class=container>
        <img src='/splurt/logo'  alt="Radio station logo">
        <h1>Graph of line-in audio</h1>
        <img src="/tmp/splurt/lineincheck.png" />
    </div>
</body>
</html>
"""
print (webPage)

#start running analysis
analysisString = "rec -c 2 -b 8 -r 4000 /tmp/splurt/lineincheck.dat stat trim 0 " + lineInCheckPeriod + " > /tmp/splurt/lineincheck.info 2>&1"
subprocess.call(analysisString,shell=True)


# execute plot
subprocess.call('nice -n 19 gnuplot /usr/share/splurt/lineincheck.gpi',shell=True)

