#!/usr/bin/python3		
import sys, os, subprocess, cgi, cgitb, re
cgitb.enable()
from configobj import ConfigObj

# get command line parameters
config = ConfigObj('/etc/splurt/splurt.conf')

# set variables
q = cgi.FieldStorage()
workdir = config['workdir']
recdir = config['pcastwork']
workdir = config['workdir']
cutPodcasts = config['cutPodcasts']
cutPodcastDir = workdir + cutPodcasts
maxPodcastCuts = config['maxPodcastCuts']
podcastFile = q.getvalue("file")
i = 1
sn = "start" + str(i)
finishn = "finish" + str(i)
descn = "desc" + str(i)
createdFileInformation="Attempted to create:"

print ("Content-type:text/html;")
print ("")
webPage = """
<!doctype html>
<html>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<link rel="stylesheet" href="/javascript/bootstrap/css/bootstrap.min.css">
	<script src="/usr/javascript/jquery/jquery.min.js"></script>
	<script src="/javascript/bootstrap/js/bootstrap.min.js"></script>
	<link rel='stylesheet' type='text/css' href='/splurt/html/styles.css'>
	<title>Podcasting while on air</title>
</head>
<body>
	<script type=\"text/javascript\" src=\"http://www.google.com/jsapi\"></script>
    <div class='container'>
        <img src='/splurt/logo' alt='Radio station logo'>
        <form action='/cgi-bin/sendfiles.py' method='POST' target='_blank'>
"""
print (webPage)

while q.getvalue(sn) and i < int(maxPodcastCuts) :
    startCut = q.getvalue(sn)
    finishCut = q.getvalue(finishn)
    cutDesc = q.getvalue(descn)

    # put times into mp3splt format i.e. min.sec    
    if startCut.lower().find("h") != -1 :
        startCutH, startCutM = startCut.lower().split("h")
        startCut = str(float(startCutH)*60 + float(startCutM))
    else :
        startCut = str(float(startCut))

    if finishCut.lower().find("h") != -1 :
        finishCutH, finishCutM = finishCut.lower().split("h")
        finishCut = str(float(finishCutH)*60 + float(finishCutM))
    else :
        finishCut = str(float(finishCut))

    # convert description into file ready format i.e. convert special
    # characters
    stripdesc = re.sub(r'[\$\\\/\?\*\%\:\|\"\<\>\ ]', '_', cutDesc)
    fullfilename = workdir + recdir + podcastFile
    linkfile = os.path.basename(podcastFile)[:len(podcastFile)-4] + "_" + stripdesc
    cmd = 'mp3splt -Q "' + podcastFile + '" -d "' + cutPodcastDir + '" -g %[@o,@c="' + \
          cutDesc + '"] -o "' + linkfile + '" ' + startCut + ' ' + finishCut
    subprocess.call(cmd, shell=True)
    #print (cmd + "<p>")
    linkfile = linkfile + ".ogg"
    print ("\n<br><br>Created file <a \
                 href=\"/cut_podcasts/{}\">{} \
                 (download)</a>".format(linkfile, cutDesc))
    print ("<input type='hidden' value=\"/cut_podcasts/{}\" \
                 name=\"File_{}\"/>".format(linkfile,str(i)))
    print ("<input type='hidden' value=\"{}\" \
        name=\"Desc_{}\"/>".format(cutDesc, str(i)))
    createdFileInformation = createdFileInformation + "\n   Filename = " + \
                 linkfile + "\n   Starttime = " + startCut + "\n \
                 Finishtime = " + finishCut + "\n   Description = " + \
                 cutDesc + "\n"
    i += 1
    sn = "start" + str(i)
    finishn = "finish" + str(i)
    descn = "desc" + str(i)
createdFileInformation = createdFileInformation + "\n\nIf you don't get these files delivered to you, go back <a href=\'/cut_podcasts/\'>here</a> and find your podcasts for downloading.\n"

webPage = """
		<input type='hidden' value="{}" name='bodyOfEmail'/>
		<p>Take these files now; they will be automatically deleted in two days.  To take them, please click on the word <i>download</i> beside each listed file, and select \"Save as...\" or \"Save Target as...\" and save them somewhere on your PC.</b>
		<p><i>Alternatively,supply your email address here and your files will be e-mailed to you in the fullness of time.  Any files over 5Mb are unlikely to get through, so if your podcasts are more than then minutes, maybe you ought to manually download them.</i>
		<p>If you don't get these files delivered to you, go back <a href='/cut_podcasts/'>here</a> and find your podcasts for downloading.
		<p><b>E-mail address </b><input type='text'  name='email' </input><br>
		<button name='submit' type='submit'>Submit</button>
	</form>
</div>
</body>
</html>
""".format(createdFileInformation)
print (webPage)
