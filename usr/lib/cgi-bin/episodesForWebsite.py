#!/usr/bin/python3
import  datetime, cgi, cgitb, re
cgitb.enable()
from configobj import ConfigObj
from pathlib import Path

# get command line parameters
config = ConfigObj('/etc/splurt/splurt.conf')
podcastDir = config['workdir'] + config['pcastwork']
arguments = cgi.FieldStorage()
server = arguments.getvalue('server')
show = arguments.getvalue('show')

# write webpage
print ("Content-type:text/html")
print ("")

# find all files
searchPath = "*{}*.ogg".format(show)
for path in sorted(list(set(Path(podcastDir).rglob(searchPath))), reverse=True):
    foundDateInName = re.search("20[0-9]{2}-[0-9]{2}-[0-9]{2}", path.name)
    if foundDateInName:
        airDate = datetime.datetime.strptime(foundDateInName.group(),'%Y-%m-%d').strftime('%A, %B %d, %Y')
        print("<p class=\"show-dates text-center\">{}</p>".format(airDate))
        print("<table class=\"table\"><tbody><tr><td><p class=\"show-dates text-center\">All non-Apple devices (OGG)</p></td><td><audio src=\"{}\" controls preload=\"none\"></audio></td></tr>".format(server + str(path).replace(config['workdir'],"")))
        aacFileName = path.with_suffix('.aac')
        aacFilePath = Path(aacFileName)
        if aacFilePath.is_file():
            print("<tr><td><p class=\"show-dates text-center\">Apple devices (AAC)</p></td><td><audio src=\"{}\" controls title=\"AAC\" preload=\"none\"> </audio></td></tr>".format(server + str(aacFileName).replace(config['workdir'],"")))
        print("</tbody></table>")


