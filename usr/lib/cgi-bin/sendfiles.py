#!/usr/bin/python3
import sys, os, subprocess, cgi, cgitb
cgitb.enable()
from configobj import ConfigObj

# get command line parameters
config = ConfigObj('/etc/splurt/splurt.conf')

# set variables
q = cgi.FieldStorage()
workdir = config['workdir']
cutPodcasts = config['cutPodcasts']
hdr = workdir + cutPodcasts

print ("Content-type:text/html")
print ("")
webPage = """
<!doctype html>
<html>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<link rel="stylesheet" href="/javascript/bootstrap/css/bootstrap.min.css">
	<script src="/usr/javascript/jquery/jquery.min.js"></script>
	<script src="/javascript/bootstrap/js/bootstrap.min.js"></script>
	<link rel='stylesheet' type='text/css' href='/splurt/html/styles.css'>
	<title>Sending files</title>
</head>
<body>
    <div class='container'>
        <img src='/splurt/logo' alt='Radio station logo'>
"""
i = 1
while q.getvalue("File_" + str(i)) :
    cmd =  'mutt -s "Sent podcast file ' + q.getvalue("Desc_" + \
            str(i)) + '" ' + q.getvalue("email") + '  -a "' + workdir + \
            q.getvalue("File_" + str(i)) + '" < /dev/null'
    print ("<br>Sent : " + q.getvalue("File_" + str(i)))
    subprocess.call(cmd, shell=True)
    i += 1
cmd =  'echo "' + str(q.getvalue("bodyOfEmail")) + '" | mutt -s "Summary" ' + q.getvalue("email")
subprocess.call(cmd, shell=True)
i -= 1
webPage = """
<p><br>{} files sent
</div>
</body>
</html>
""".format(i)
print (webPage)
