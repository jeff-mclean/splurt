#!/usr/bin/python3
import os, subprocess, cgi, cgitb
cgitb.enable()
from configobj import ConfigObj

# get command line parameters
config = ConfigObj('/etc/splurt/splurt.conf')
constantSeconds = config['constantSeconds']
constantPTSCF = config['constantPTSCF']
constantNOPTSBTA = config['constantNOPTSBTA']
constantTRL = config['constantTRL']
arguments = cgi.FieldStorage()
audioFile = arguments.getvalue('file')                                            # the sound file to be analysed and cut for voice
seconds = int(arguments.getvalue('seconds', constantSeconds))                                      # the number of seconds in an analysis period
periodsToStartCutFrom = int(arguments.getvalue('backchunks', constantPTSCF))                     # when the audio goes under a threshold for the number of periods specified in the next parameter, we need to skip back this number of periods to mark where we start the recording 
numberOfPeriodsToSustainBeforeTakingAction = int(arguments.getvalue('cchunks', constantNOPTSBTA))   # number of periods that the audio is either over or under the RMS value before we take action 
thresholdLevel = float(arguments.getvalue('thresholdLevel', constantTRL))                         # the RMS level that determines what might be voice (below) and music (above) 
cutPodcasts = config['cutPodcasts']							# this is the directory that cut podcasts are put into
workdir = config['workdir']
aacp = config['apacheAliasCutPodcasts']							# this is the apache alias setup in the apache2 config file for splut for where to find the cut podcasts
soundPodcastRate = config['soundPodcastRate']						# this is the file that splits segments apart in the combined autocut sound file for the programme.  Usually white noise
soundPodcastChannels = config['soundPodcastChannels']						# this is the file that splits segments apart in the combined autocut sound file for the programme.  Usually white noise

#set variables
audioFileName = os.path.basename(audioFile)
subprocess.call("mkdir -p /tmp/splurt", shell=True)
fobj_cl = open("/tmp/splurt/clfile", "w")
fobj_gpi = open("/tmp/splurt/audio.gpi", "w")
oopsFile = "/tmp/splurt/oopsfile.ogg"
#noiseFile = "/var/lib/splurt/cut_podcasts/noise.ogg"
noiseFile = "/tmp/splurt/noise.ogg"
i = 1
voicePeriods = 0
musicPeriods = 0
sample = 0
status = ""
lastAction = ""
segment = 0
generateLinkToSegmentFile = False
combineCommand = ""

#generate noise for separator
genNoiseCmd = "sox -c " + soundPodcastChannels + " -r " + soundPodcastRate + " -n " + noiseFile + " synth 1 whitenoise"
subprocess.call("nice -n 19 " + genNoiseCmd, shell=True)

#start writing webpage
print ("Content-type:text/html")
print ("")
webPage = """
<!doctype html>
<html>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<link rel="stylesheet" href="/javascript/bootstrap/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="/javascript/bootstrap/js/bootstrap.min.js"></script>
	<link rel='stylesheet' type='text/css' href='/splurt/html/styles.css'>
	<title>Automatic analysis of file</title>
</head>
<body>
<h1>Source and other files</h1>
	<a href="/podcasts/{0}">{0}</a><br>
<h1>Analysis tables</h1>
<div class="container" >
    <img src='/splurt/logo' alt='Radio station logo'>
	<table class="table">
		<thead>
			<tr>
				<th>Period</th>
				<th>Time</th>
				<th>Status</th>
				<th>Over</th>
				<th>Under</th>
				<th>Action</th>
				<th>Value</th>
				<th>Cut command</th>
				<th>Resultant file</th>
			</tr>
		</thead>
		<tbody>
"""
print (webPage.format(audioFileName))

#start running analysis
analysisString = "nice -n 19 sox " + audioFile + ' -n trim 0 ' + str(seconds) + ' oops stat stats : restart > /tmp/splurt/analysis 2>&1'
#subprocess.call(analysisString, shell=True)

f1  = open("/tmp/splurt/rmsAmp", "w")
f2  = open("/tmp/splurt/midlineAmp", "w")
f3  = open("/tmp/splurt/roughFreq", "w")
f4  = open("/tmp/splurt/rmsDelta", "w")
f5  = open("/tmp/splurt/meanAmp", "w")
f6  = open("/tmp/splurt/meanDelta", "w")
f7  = open("/tmp/splurt/meanNorm", "w")
f8  = open("/tmp/splurt/maxDelta", "w")
f9  = open("/tmp/splurt/dcOffset", "w")
f10 = open("/tmp/splurt/rmsLevDb", "w")
f11 = open("/tmp/splurt/rmsPkDb", "w")
f12 = open("/tmp/splurt/rmsTrDb", "w")
f13 = open("/tmp/splurt/crestFactor", "w")
f14 = open("/tmp/splurt/flatFactor", "w")
f15 = open("/tmp/splurt/pkCount", "w")
f16 = open("/tmp/splurt/windowS", "w")
splitInfo =[
   ["RMS     amp",       f1,  1, 0 ],
   ["Midline amplitude", f2,  1, 1 ],
   ["Rough",             f3,  1, 2 ],
   ["RMS     delta",     f4,  1, 3 ],
   ["Mean    amplitude", f5,  1, 4 ],
   ["Mean    delta",     f6,  1, 5 ],
   ["Mean    norm",      f7,  1, 6 ],
   ["Maximum delta",     f8,  1, 7 ],
   ["DC offset",         f9,  1, 8 ],
   ["RMS lev dB",        f10, 1, 9 ],
   ["RMS Pk dB",         f11, 1, 10],
   ["RMS Tr dB",         f12, 1, 11],
   ["Crest factor",      f13, 1, 12],
   ["Flat factor",       f14, 1, 13],
   ["Pk count",          f15, 1, 14],
   ["Window s",          f16, 1, 15],
]
fobj_analysisFile = open("/tmp/splurt/analysis", "r")
for line in fobj_analysisFile:
    for (key, file_obj, period, index) in splitInfo:
        if line.startswith(key):
            file_obj.write("{:02d}:{:02d}:{:02d} {}".format(int(period*seconds/60/60%24), int(period*seconds/60%60), int(period*seconds%60), line.split(" ")[-1]))
            splitInfo[index][2]+=1
f1.close()
f2.close()
f3.close()
f4.close()
f5.close()
f6.close()
f7.close()
f8.close()
f9.close()
f10.close()
f11.close()
f12.close()
f13.close()
f14.close()
f15.close()
f16.close()

# start writing gpi out
stringToWrite = """
#set output format and size
set term png size 2000,500

# we want just the data
set xdata time
set timefmt "%%H:%%M:%%S"
set format x "%%H:%%M:%%S"


# draw data with foreground color
set output "/tmp/splurt/rmsAmp.png"
plot "/tmp/splurt/rmsAmp" using 1:2 title "RMS amplitude" with lines
set output "/tmp/splurt/roughFreq.png"
plot "/tmp/splurt/roughFreq" using 1:2 title "Rough frequency" with lines
set output "/tmp/splurt/rmsDelta.png"
plot "/tmp/splurt/rmsDelta" using 1:2 title "RMS Delta" with lines
set output "/tmp/splurt/meanDelta.png"
plot "/tmp/splurt/meanDelta" using 1:2 title "Mean Delta" with lines
set output "/tmp/splurt/meanNorm.png"
plot "/tmp/splurt/meanNorm" using 1:2 title "Mean norm" with lines, {:.2f} with lines
set output "/tmp/splurt/maxDelta.png"
plot "/tmp/splurt/maxDelta" using 1:2 title "Max Delta" with lines
set output "/tmp/splurt/midlineAmp.png"
plot "/tmp/splurt/midlineAmp" using 1:2 title "Midline amplitude" with lines
set output "/tmp/splurt/meanAmp.png"
plot "/tmp/splurt/meanAmp" using 1:2 title "Mean amplitude" with lines
set output "/tmp/splurt/dcOffset.png"
plot "/tmp/splurt/dcOffset" using 1:2 title "DC Offset" with lines
set output "/tmp/splurt/rmsLevDb.png"
plot "/tmp/splurt/rmsLevDb" using 1:2 title "RMS Level dB" with lines
set output "/tmp/splurt/rmsPkDb.png"
plot "/tmp/splurt/rmsPkDb" using 1:2 title "RMS Peak dB" with lines
set output "/tmp/splurt/rmsTrDb.png"
plot "/tmp/splurt/rmsTrDb" using 1:2 title "RMS Tr dB" with lines
set output "/tmp/splurt/crestFactor.png"
plot "/tmp/splurt/crestFactor" using 1:2 title "Crest Factor" with lines
set output "/tmp/splurt/flatFactor.png"
plot "/tmp/splurt/flatFactor" using 1:2 title "Flat Factor" with lines
set output "/tmp/splurt/pkCount.png"
plot "/tmp/splurt/pkCount" using 1:2 title "Peak count" with lines
set output "/tmp/splurt/windowS.png"
plot "/tmp/splurt/windowS" using 1:2 title "Window s" with lines
""".format(thresholdLevel)

fobj_gpi.write(stringToWrite)
fobj_gpi.close()

# execute plot
#subprocess.call('nice -n 19 gnuplot /tmp/splurt/audio.gpi', shell=True)

startCutTime = "0"
fobj_analysisFile = open("/tmp/splurt/meanNorm", "r")
for line in fobj_analysisFile:
    lineTime, lineValue = line.split(' ', 1)
    m, s = divmod(i*seconds, 60)
    timeStr = "{:03d}.{:02d}".format(m, s)
    possibleStart = i*seconds-seconds*periodsToStartCutFrom
    if possibleStart < 0:
        possibleStartTime = "0.0"
    else:
        m, s = divmod(i*seconds-seconds*periodsToStartCutFrom, 60)
        possibleStartTime = "{:03d}.{:02d}".format(m, s)
    sample += 1
    action = ""
    if float(lineValue) < thresholdLevel:
        #if float(lineValue) == 0:
        status = "voice"
    else:
        status = "music"
    if status == "voice":
        musicPeriods = 0
        voicePeriods += 1
    if status == "music":
        voicePeriods = 0
        musicPeriods += 1
    if voicePeriods == numberOfPeriodsToSustainBeforeTakingAction and lastAction != "startVoice":
        action = "start cut at " + possibleStartTime + " "
        lastAction = "startVoice"
        startCutTime = possibleStartTime
    cutCommand=""
    segmentFile=""
    if musicPeriods == numberOfPeriodsToSustainBeforeTakingAction and lastAction == "startVoice":
        segment += 1
        action = "stop cut here   "
    if float(startCutTime) < 0 :
        startCutTime = "0"
    segmentFile = audioFileName + "-" + startCutTime + "-" + timeStr + "-Segment-" + str(segment)
    oopsSegmentFile = os.path.basename(oopsFile) + "-" + startCutTime + "-" + timeStr + "-Segment-" + str(segment)
    cutCommand = "mp3splt -Q " + audioFile + " " + startCutTime + " " + str(timeStr) + " -o " + segmentFile + " -d " + cutPodcasts
    # print ("CUTTING => " + cutCommand + "<br>")
    fobj_cl.write(cutCommand + "\n")
    if segment == 1:
        combineCommand = "sox " + cutPodcasts + segmentFile + ".ogg	 "
    else:
        combineCommand = combineCommand + " " + noiseFile + " " + cutPodcasts + segmentFile + ".ogg "
        lastAction = "stopVoice"
        generateLinkToSegmentFile = True
    if generateLinkToSegmentFile:
        print ("\n<tr><td>" + str(i) + "</td><td>" + lineTime + "</td><td>" + str(status) + "</td><td>" + str(musicPeriods) + "</td><td>" + str(voicePeriods) + "</td><td>" + action + "</td><td>" + lineValue + "</td><td></td><td><a href=\"{}{}.ogg\">Segment file number {}</a><br></td></tr>".format(cutPodcasts, segmentFile, segment))
        generateLinkToSegmentFile = False
    #else:
        print ("\n<tr><td>" + str(i) + "</td><td>" + lineTime + "</td><td>" + \
            str(status) + "</td><td>" + str(musicPeriods) + "</td><td>" + \
            str(voicePeriods) + "</td><td>" + action + "</td><td>" + lineValue + \
            "</td><td>" + cutCommand + "</td><td></td></tr>")
    i += 1
fobj_analysisFile.close()
fobj_cl.close()
combineCommand = combineCommand + " " + workdir + cutPodcasts + audioFileName + "_combined.ogg"

# execute cutfile lines
subprocess.call("nice -n 19 bash /tmp/splurt/clfile", shell=True)

# combine all segments and provide link to it
if segment > 0 :
    print ("<br><br>" + combineCommand + "<br>")
    subprocess.call("nice -n 19 " + combineCommand, shell=True)
    print ("\n<tr><td>Combined</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><a href=\"{0}{1}_combined.ogg\">Combined file</a><br></td></tr>".format(aacp, audioFileName))
else:
    print ("\n<tr><td>There were no segments created.</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>")

# finish off html
webPage = """
		</tbody>
	</table>
</div>
<h1>Graphs</h1>
	<h2>RMS Analysis</h2>
	<img src="/tmp/splurt/rmsAmp.png" />
	<h2>Rough Frequency</h2>
	<img src="/tmp/splurt/roughFreq.png" />
	<h2>RMS Delta</h2>
	<img src="/tmp/splurt/rmsDelta.png" />
	<h2>Mean Delta</h2>
	<img src="/tmp/splurt/meanDelta.png" />
	<h2>Mean normalised</h2>
	<img src="/tmp/splurt/meanNorm.png" />
	<h2>Max Delta</h2>
	<img src="/tmp/splurt/maxDelta.png" />
	<h2>Midline Amplitude</h2>
	<img src="/tmp/splurt/midlineAmp.png" />
	<h2>Mean Amplitude</h2>
	<img src="/tmp/splurt/meanAmp.png" />
	<h2>DC Offset</h2>
	<img src="/tmp/splurt/dcOffset.png" />
	<h2>RMS Level dB</h2>
	<img src="/tmp/splurt/rmsLevDb.png" />
	<h2>RMS Peak dB</h2>
	<img src="/tmp/splurt/rmsPkDb.png" />
	<h2>RMS Tr dB</h2>
	<img src="/tmp/splurt/rmsTrDb.png" />
	<h2>Crest factor</h2>
	<img src="/tmp/splurt/crestFactor.png" />
	<h2>Flat factor</h2>
	<img src="/tmp/splurt/flatFactor.png" />
	<h2>Peak count</h2>
	<img src="/tmp/splurt/pkCount.png" />
	<h2>WindowS</h2>
	<img src="/tmp/splurt/windowS.png" />
</body>
</html>
"""
print (webPage)
