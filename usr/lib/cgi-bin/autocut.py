#!/usr/bin/python3
import sys, os, subprocess, cgi, cgitb, mutagen
cgitb.enable()
from configobj import ConfigObj

# get command line parameters
config = ConfigObj('/etc/splurt/splurt.conf')
workDir = config['workdir']

# set variables
q = cgi.FieldStorage()
workingDir = config['workdir']
podcastDir = config['pcastwork']
constantSeconds = config['constantSeconds']
constantPTSCF = config['constantPTSCF']
constantNOPTSBTA = config['constantNOPTSBTA']
constantTRL = config['constantTRL']
inputmodifier = q.getvalue("fname")
fileList = []

# create page
print ("Content-type:text/html")
print ("")
webPage = """
<!doctype html>
<html>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<link rel="stylesheet" href="/javascript/bootstrap/css/bootstrap.min.css">
	<script src="/javascript/jquery/jquery.min.js"></script>
	<script src="/javascript/bootstrap/js/bootstrap.min.js"></script>
	<link rel='stylesheet' type='text/css' href='/splurt/html/styles.css'>
	<title>Automatic cutting of show into podcast sections</title>
</head>
<body>
	<script type=\"text/javascript\" src=\"http://www.google.com/jsapi\"></script>
	<div class='container'>
        <img src='/splurt/logo' alt='Radio station logo'>
"""
print (webPage)

for r, d, f in os.walk(workingDir + podcastDir) :
    for item in f :
        if inputmodifier == None  or item.lower().find(inputmodifier.lower()) !=-1 :
            fileList.append(r + "/" + item)
fileList.sort()

if len(fileList) != 0 :
    webPage = """
        <h1>Which one of the following files do you want to make podcasts from:</h1>
        <form role="form" action='/cgi-bin/cutlist.py' method='POST' target='_blank'>
    """
    print (webPage)

    for fileName in fileList :
        fileInfo = mutagen.File(fileName)
        print ("<input type='radio'  name='file' value=\"{}\">{}, \
            {}</input><br>\n".format(fileName,fileInfo["title"],fileInfo["date"]))

    # print cut values
    webPage = """
    <br><h1>Please type in your values for the auto-cut<br></h1>
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text">Number of seconds that constitute an anlysis chunk</span>
        </div>
        <input type="text" class="form-control" name="seconds" placeholder="{}">
      </div>

      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text">Number of chunks to go back when we think we should start a cut</span>
        </div>
        <input type="text" class="form-control" name="backchunks" placeholder="{}">
      </div>

      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text">Number of chunks to determine whether cut should start or stop</span>
        </div>
        <input type="text" class="form-control" name="cchunks" placeholder="{}">
      </div>

      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text">Threshold RMS level</span>
        </div>
        <input type="text" class="form-control" name="thresholdLevel" placeholder="{}">
      </div>

      <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
          <button type="submit" class="btn btn-default">Submit</button>
        </div>
      </div>
      </form>
    """
    print (webPage.format(constantSeconds, constantPTSCF, constantNOPTSBTA,
                          constantTRL))
else :
    webPage = """
        <p>No programmes were found with "{}" in the name.  Please go back and
        try again. <p>
    """.format(inputmodifier)
    print (webPage)

#print trailer
webPage = """
</div>
</body>
</html>
"""
print (webPage)
