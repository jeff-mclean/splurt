#!/usr/bin/python3
import sys, os, subprocess, cgi, cgitb
cgitb.enable()
from configobj import ConfigObj

# get config parameters
config = ConfigObj('/etc/splurt/splurt.conf')
workdir = config['workdir']
exclude = config['progForLogs']

# set variables
q = cgi.FieldStorage()
oldread = ""
code1 = ""
name1 = ""
line = ""
recs = 0

if not q.getvalue("Show") :
	cmd = "cut -f 2,3,4,6,7,8 -d '~' " + workdir + \
    "/program-access-summary-for-analysis.csv | grep -v \"" + exclude + "\" > /tmp/tmpfilelistener"
else :
	cmd = "cut -f 2,3,4,6,7,8 -d '~' " + workdir + \
    "/program-access-summary-for-analysis.csv | grep -i \"" + q.getvalue("Show") + "\" > /tmp/tmpfilelistener"
subprocess.call(cmd,shell=True)

print ("Content-type:text/html")
print ("")
webPage = """
<!doctype html>
<html>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<link rel="stylesheet" href="/javascript/bootstrap/css/bootstrap.min.css">
	<script type=\"text/javascript\" src=\"http://www.google.com/jsapi\"></script>
	<script type=\"text/javascript\"></script>
	<link rel='stylesheet' type='text/css' href='/splurt/html/styles.css'>
"""
print (webPage)

if os.stat("/tmp/tmpfilelistener").st_size == 0 :
    webPage = """
        <title>Listener analysis (ICES stream)</title>
    </head>
    <body>
        <div class='container'>
            <img src='/splurt/logo' alt='Radio station logo'>
            <h1>There is nothing to show here.</h1>  You might be using pulse for
            accessing audio.  <p>
            Please visit the <a href="https://admin.nucleusstreaming.com">Pulse admin
            pages</a> for more info (if
            you have been given authority to access it.)
        </div>
    """
    print (webPage)
else :
    tempfile = open("/tmp/tmpfilelistener", "r")
    webPage = """
    google.load('visualization', '1', {packages: ['table', 'controls']})
    function drawVisualization()  :
    """
    print (webPage)
    for line in tempfile :
        prg[recs], avg_mins[recs], num_conns[recs], tot_mins[recs], tot_hrs[recs], x[recs] = line.split('~')
        x[recs].rstrip("\n")
        recs += 1

    # Create and populate the data table.
    webPage = """
    var data = new google.visualization.DataTable()
    data.addColumn('string', 'Time of connection')
    data.addColumn('number', 'Number of minutes of connection')
    data.addColumn('string', 'Source country')
    data.addColumn('string', 'Name of show')
    data.addColumn('string','Date of show')
    data.addRows([
    """
    print (webPage)
    while recs > 0 :
        recs -= 1
    string = "   [\"{}\",{}, \"{}\", \"{}\", \"{}\"]".format(prg[recs], num_conns[recs],
                                                tot_mins[recs], tot_hrs[recs],
                                                x[recs])
    if recs > 0 :
        string = string + ","
    print (string)

    # Create and draw the visualizati\non
    webPage = """
    var stringFilter = new google.visualization.ControlWrapper( :
    'controlType': 'StringFilter',
    'containerId': 'control1',
    'options':  :
      'filterColumnLabel': 'Name of show'
    }
    })

    var grouped_dt = google.visualization.data.group(
      data, [3,4,2]
      ,[{'column': 1, 'aggregation': google.visualization.data.sum, 'type': 'number', 'label': 'Total minutes'}]
      ,[{'column': 1, 'aggregation': google.visualization.data.count, 'type': 'number', 'label': 'Number of connections'}]
      )


    var grouped_dt2 = google.visualization.data.group(
      data, [3,4]
      ,[{'column': 1, 'aggregation': google.visualization.data.sum, 'type': 'number', 'label': 'Total minutes'}]
      )

    var grouped_dt2a = google.visualization.data.group(
      data, [3,4]
      ,[{'column': 1, 'aggregation': google.visualization.data.count, 'type': 'number', 'label': 'Number of connections'}]
      )

    var grouped_dt2j = google.visualization.data.join(
      grouped_dt2, grouped_dt2a, 'inner', [[0,0], [1,1]], [2], [2]
      )




    var grouped_table = new google.visualization.Table(document.getElementById('grouped_table'))
    grouped_table.draw(grouped_dt, null);


    var grouped_table2 = new google.visualization.Table(document.getElementById('grouped_table2'))
    grouped_table2.draw(grouped_dt2j, null);\n


    var table = new google.visualization.Table(document.getElementById('visualization'))


    table.draw(data, {sortColumn: 2, allowHtml: true, showRowNumber: true})
    }
    google.setOnLoadCallback(drawVisualization)
    </script>
    </head>
    <body style=\"font-family: Arial;border: 0 none;\">
        <div class='container'>
            <img src='/splurt/logo' alt="Radio station logo"><p>
            <h1>Splurt Streaming Listener Analysis</h1>
            <h2>Contents</h2>
            <a href=\"#totref\"><p>Total Minutes per show</a>
            <a href=\"#totrefc\"><p>Total Minutes per show per country</a>
            <a href=\"#totrefa\"><p>All connections</a>
            <h2>Analysis</h2>
            <a name=\"totref\"><h2>This table shows the total number of minutes of connection per show, broken down by date.</h2></a>
            <div id=\"grouped_table2\"></div>
            <p>
            <a name=\"totrefc\"><h2>This table shows the total number of minutes of connection per show from each country, also broken down by date.</h2></a>
            <div id=\"grouped_table\"></div>
            <p>
            <a name=\"totrefa\"><h2>This table shows all individual connections to the streaming service.</h2></a>
            <div id=\"visualization\"></div>
        </div>
    """
    print (webPage)
webPage = """
</body>
</html>
"""
print (webPage)
