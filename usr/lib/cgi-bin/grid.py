#!/usr/bin/python3
import sys, os, subprocess, cgi, cgitb, urllib, time
cgitb.enable()
from configobj import ConfigObj
from datetime import datetime

# get command line parameters
config = ConfigObj('/etc/splurt/splurt.conf')
workDir = config['workdir']

# set variables
q = cgi.FieldStorage()
progsDataFile = config['progsDataFile']


progsFile = open(progsDataFile, 'r')
oldpday = "0"
days = ["","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
stformat = "%Y-%m-%d-%H-%M-%S"
modified_time = os.stat(progsDataFile).st_mtime

print ("Content-type:text/html")
print ("")
webPage = """
<!doctype html>
<html>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<link rel="stylesheet" href="/javascript/bootstrap/css/bootstrap.min.css">
	<script src="/javascript/jquery/jquery.min.js"></script>
	<script src="/javascript/bootstrap/js/bootstrap.min.js"></script>
	<link rel='stylesheet' type='text/css' href='/splurt/html/styles.css'>
	<title>SPLURT Programme Guide</title>
</head>
<body>
	<div class='container'>
        <img src='/splurt/logo'  alt="Radio station logo">
        <h1>SPLURT Programme Guide</h1>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Start Time</th>
                <th>Duration (hours)</th>
                <th>Program Name</th>
                <th>Program Announcer</th>
                <th>Program Description</th>
                <th>Recent presenter audio</th>
                <th>Recent recordings</th>
            </tr>
        </thead>
        <tbody>
"""
print (webPage)

lastDay = ""
for line in progsFile :
    pday, pstart, pdur, pprog, pdesc, ppres = line.split(';')
    showstartap = "am"
    showstart = pstart
    if int(pstart) >= 1200 :
        showstartap = "pm"
        if int(pstart) > 1200 :
            showstart = "{:04d}".format(int(pstart) - 1200)
    if int(pstart) == 0 :
        showstartap = "am"
        showstart = "1200"
    showstart = showstart[0:2] + ":" + showstart[2:4] + showstartap
    pdur = int(pdur)/3600
    if  pday != lastDay :
        lastDay = pday
        print ("\n<tr class='info'><td colspan=5>{}</td></tr>".format(days[int(pday)]))
    print ("\n<tr>\n")
    psrch = pprog
    psrch = psrch.replace(" ","-")
    psrch = urllib.parse.quote(psrch)
    webPage = """
        <td>{}</td>
        <td>{}</td>
        <td>{}</td>
        <td>{}</td>
        <td>{}</td>
        <td><a href='/cut_podcasts/?C=M;O=D;P={}*combined.ogg'>Presenter audio from recent shows</a> </td>
        <td><a href='/podcasts/?C=M;O=D;P={}*'>Recent shows</a> </td>
        </tr>
    """.format(showstart,pdur,pprog,pdesc,ppres,psrch,psrch)
    print (webPage)
progsFile.close()
webPage = """
			</tbody></table>
            <br>Grid current as of {}
</div>
</body>
</html>
""".format(time.ctime(modified_time))
print (webPage)

