#!/usr/bin/python3
import sys, os, subprocess, cgi, cgitb, mutagen
cgitb.enable()
from configobj import ConfigObj

# get command line parameters
config = ConfigObj('/etc/splurt/splurt.conf')

# set variables
q = cgi.FieldStorage()
cutPodcasts = config['cutPodcasts']
workingDir = config['workdir']
cutPodcastDir = workingDir + cutPodcasts
fileList = []

print ("Content-type:text/html")
print ("")
webPage = """
<!doctype html>
<html>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<link rel="stylesheet" href="/javascript/bootstrap/css/bootstrap.min.css">
	<script src="/javascript/jquery/jquery.min.js"></script>
	<script src="/javascript/bootstrap/js/bootstrap.min.js"></script>
	<link rel='stylesheet' type='text/css' href='/splurt/html/styles.css'>
	<title>Files</title>
</head>
<body>
	<h2>Previously created podcasts</h2>
	Click on any item below to download the relevant podcast
        <table class="table table-bordered">
                <thead>
                <tr>
                        <th>Program</th>
                        <th>Date</th>
                        <th>Segment name</th>
                </tr>
        </thead>
        <tbody>
"""
print (webPage)

inputmodifier = q.getvalue("fname")
for r, d, f in os.walk(cutPodcastDir) :
    for item in f :
        if item.find(inputmodifier) :
            fileList.append(item)
fileList.sort()
for fileName in fileList :
    print ("<tr>")
    fullFileName = cutPodcastDir + fileName
    fileInfo = mutagen.File(fullFileName)
    print ("<td><a href=\"/{}/{}\">{}</a></td>".format(cutPodcasts, fileName, fileInfo["title"]))
    print ("<td><a href=\"/{}/{}\">{}</a></td>".format(cutPodcasts, fileName, fileInfo["date"]))
    print ("<td><a href=\"/{}/{}\">{}</a></td>".format(cutPodcasts, fileName, fileInfo["comment"]))
    print ("<tr>")
webPage = """
</tbody>
</table>
<p><br>{} files previously created
</div>
</body>
</html>
""".format(len(fileList))
print (webPage)
