#!/usr/bin/python3
import sys, os, subprocess, cgi, cgitb
cgitb.enable()
from configobj import ConfigObj
from datetime import datetime

# get command line parameters
config = ConfigObj('/etc/splurt/splurt.conf')

# set variables
q = cgi.FieldStorage()
workingDir = config['workdir']
recfilenamestore = config['recFilenameStore']

print ("Content-type:text/html")
print ("")
webPage = """
<!doctype html>
<html>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
	<link rel="stylesheet" href="/javascript/bootstrap/css/bootstrap.min.css">
	<script src="/javascript/jquery/jquery.min.js"></script>
	<script src="/javascript/bootstrap/js/bootstrap.min.js"></script>
	<link rel='stylesheet' type='text/css' href='/splurt/html/styles.css'>
	<title>Podcasting while on air</title>
</head>
<body>
"""
print (webPage)
f = open(workingDir + recfilenamestore, "r")
recfilename = f.readline().strip()
f.close()
starttime = recfilename[-23:-4]
stformat = "%Y-%m-%d-%H-%M-%S"
tdelta = datetime.now() - datetime.strptime(starttime, stformat)
etime = str(tdelta)[:str(tdelta).index(".")]

webPage = """
<div class='container'>
    <img src="/splurt/logo" alt="Radio station logo"/>
    <h1>Podcasting while on air</h1>
    <form action="/cgi-bin/cutfile.py" method="post">
        <input type='hidden' name='etime' value='{etime}'/>
        <input type='hidden' name='file' value="{rfn}"/>
        The system is currently recording {rfn} <p>
        It started {etime} ago.
        <p><i><b>There are a few main things you need to know:
        <ul>
            <span style="color: #ff0000">
                <li>Make sure you only hit the Finish button BEFORE the last 5mins of your show, or 10 mins AFTER the end of your show</li>
                <li>Hit the "Add a Podcast" button when you're about to start talking on air</li>
                <li>In the next box that comes up after hitting the "Add a Podcast" button, you'll hit that button just after coming off air, then give the segment a name - generally, what you just talked about.  When you click Ok, you can then repeat the procedure for the rest of the show, and then click the "Finish for Cutting and Sending" button.  You're done!</li>
            </span>
        </ul>
        <br><i><b>If you don't heed these messages, sorry, but you won't get your podcasts delivered to you...</b></i><br>
        <p id="demo"></p>
        <script type="text/javascript" src="/splurt/html/DynamicPodcasting.js"></script>
        <input type="button" onclick="starttime()" value="Add a podcast"/>
        <input type="reset" value="Cancel"/>
        <input type="submit" value="Finish for cutting and sending"/>
    </form>
</div>
</body>
</html>
""".format(etime=etime, rfn=recfilename)
print (webPage)

