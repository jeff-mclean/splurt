#!/bin/bash

. /etc/splurt/splurt.conf

set -v 
nice zcat /var/log/icecast2/access.log.13.gz > /tmp/combined_access.log
nice zcat /var/log/icecast2/access.log.12.gz >> /tmp/combined_access.log
nice zcat /var/log/icecast2/access.log.11.gz >> /tmp/combined_access.log
nice zcat /var/log/icecast2/access.log.10.gz >> /tmp/combined_access.log
nice zcat /var/log/icecast2/access.log.9.gz >> /tmp/combined_access.log
nice zcat /var/log/icecast2/access.log.8.gz >> /tmp/combined_access.log
nice zcat /var/log/icecast2/access.log.7.gz >> /tmp/combined_access.log
nice zcat /var/log/icecast2/access.log.6.gz >> /tmp/combined_access.log
nice zcat /var/log/icecast2/access.log.5.gz >> /tmp/combined_access.log
nice zcat /var/log/icecast2/access.log.4.gz >> /tmp/combined_access.log
nice zcat /var/log/icecast2/access.log.3.gz >> /tmp/combined_access.log
nice zcat /var/log/icecast2/access.log.2.gz >> /tmp/combined_access.log
nice cat /var/log/icecast2/access.log.1 >> /tmp/combined_access.log
nice cat /var/log/icecast2/access.log >> /tmp/combined_access.log
/usr/share/splurt/listeners.py $workdir/program-access-summary-for-analysis.csv $progsDataFile </tmp/combined_access.log
