#!/usr/bin/python3
import sys, os, time, requests, subprocess, datetime, pdb, psutil
from configobj import ConfigObj

class Programme:
    showName = ""
    presenterName = ""
    showDesc = ""
    quality = ""
    remaining = 0
    duration = 0

class Grid:
    from configobj import ConfigObj

    programmes = []
    configFile = ""

    # get configuration parameters
    def __init__(self, file='/etc/splurt/splurt.conf'):
        import csv

        self.configFile = file
        config = ConfigObj(self.configFile)
        progsDataFile = config['progsDataFile']
        with open(progsDataFile, newline="") as f :
            reader = csv.DictReader(f, fieldnames = [ "day", "time", "duration", "name", "presenter", "description" ], delimiter = ";")
            for p in reader :
                self.programmes.append( [ p["day"], p["time"], p["duration"], p["name"], p["presenter"], p["description"] ] )

    def getProgram(self, dayAndTime):
        config = ConfigObj(self.configFile)
        progForLogs = config['progForLogs']

        # split up day and time
        day = dayAndTime[:1]
        hour = dayAndTime[1:3]
        minute = dayAndTime[3:5]
        time = dayAndTime[1:]
        # print ("\n\nincoming day and time: dayandtime = {}, day = {}, hour = {}, minute = {}, time = {}".format(dayAndTime, day, hour, minute,
        #                                      time))

        programme = Programme()
        row = 1
        found = 0
        for p in self.programmes :
            # print ("searching for program: row = {}, day + time = {}, prog day \
            #       = {}, prog time = {}, prog dur = {}, prog name = {}, prog \
            #       presenter = {}, prog desc = {}".format(row, day + time,
            #                                              p[0], p[1], p[2],
            #                                              p[3], p[4], p[5]))

            # strip leading and trailing spaces from string values
            p = list(map(str.strip, p))
            
            if row == 1 and day + time < p[0] + p[1] :
                # we are earlier than the first program, so add 7 days to it and see if we pick it up from the last program
                day = str(int(day)+7)
                # print ("time too early for first programme - adding seven days - day now equal to ", day)

            # calculate the difference between now and the start of the programme in minutes
            diffminutes = (int(day)-int(p[0]))*60*24 + (int(hour)-int(p[1][:2]))*60 + (int(minute)-int(p[1][2:]))
            durminutes = int(p[2])/60
            # print ("diffminutes and durminutes = ", diffminutes, durminutes)
            if diffminutes < durminutes and diffminutes >= 0 :
                found = 1
                programme.showName = p[3]
                programme.duration = int(p[2])+300
                programme.remaining = int(p[2])+300 - (diffminutes*60)
                programme.showDesc = p[4]
                programme.presenterName = p[5]
                if programme.showName.lower() in progForLogs.lower() :
                    programme.quality = "L"
                else :
                    programme.quality = "H"
                break
            row += 1
        if not found :
            programme.showName = ""
            programme.duration = 3595
            programme.remaining = 3595
            programme.showDesc = ""
            programme.presenterName = ""
        # print ("at end of get programme : ", locals())
        return programme

