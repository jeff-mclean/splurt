#!/usr/bin/python3
import sys, os, requests, datetime, time, operator
from configobj import ConfigObj

# get command line parameters
config = ConfigObj('/etc/splurt/splurt.conf')
pulseStation = config['pulseStation']
progsFromPulse = config['progsFromPulse']

# ask pulse for schedule
r = requests.get('http://program.nucleusstreaming.com/api/stations/' + pulseStation + '/schedule')

# write simple splurt format
schedule = r.json()
weekSchedule = []
weekTime = datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(7)
programFile = open(progsFromPulse,"w")
for program in schedule :
    if program['status'] == "SCHEDULED" :
        showTime = datetime.datetime.strptime(program['startTime'][:-3]+"00","%Y-%m-%dT%H:%M:%S%z")
        if showTime < weekTime :
            weekSchedule.append( [ showTime.weekday()+1,
                showTime.strftime("%H%M"),
                int(program['totalDuration']),
                program['programme']['name'].replace(';',' '),
                program['programme']['presenter'].replace(';',' '),
                program['programme']['description'].rstrip().replace('\n',' ').replace(';',' ') ] )
weekSchedule.sort( key = operator.itemgetter(0,1))
for program in weekSchedule :
    print (*program, sep=";")
    print (*program, sep=";", file=programFile)
