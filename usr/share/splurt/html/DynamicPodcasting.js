var begin=new Date();
var oForm=document.forms[0];
var elapsedRecordingTimeWhenPageLoaded=oForm.elements["etime"].value;
var elapsedHMS=elapsedRecordingTimeWhenPageLoaded.split(":");
var podcastNumber=1;
if (elapsedHMS.length==3){
  begin.setTime(begin-(elapsedHMS[0]*3600000+elapsedHMS[1]*60000+elapsedHMS[2]*1000));
}
if (elapsedHMS.length==2){
  begin.setTime(begin-(elapsedHMS[0]*60000+elapsedHMS[1]*1000));
}
//document.getElementById("demo").innerHTML+=begin.toLocaleString()+"<br><br>";
function pad(number, length) {
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
    return str;
}
function returnpctime(msecs){
  hrs=Math.floor(msecs/3600000);
  msecs-=hrs*3600000;
  mns=Math.floor(msecs/60000);
  mns=pad(mns,2);
  msecs-=mns*60000;
  scs=Math.floor(msecs/1000);
  scs=pad(scs,2);
  return hrs+"h"+mns+"."+scs;
}
function starttime(){
  var atime= new Date();
  var jpa= new Date();
  jpa.setTime(atime-begin);
  //Note: this will always be the reason why the podcast timing looks really bad
  jpa-=5000;
  document.getElementById("demo").innerHTML += "Segment number "+podcastNumber+"  <input type='text' name='start"+podcastNumber+"' value='"+returnpctime(jpa)+"'/>";
  var stp=alert("Press here to stop");
  var btime=new Date();
  var jpb= new Date();
  jpb.setTime(btime-begin);
  document.getElementById("demo").innerHTML += "  <input type='text' name='finish"+podcastNumber+"' value='"+returnpctime(jpb)+"'/>";
  var podcastName=prompt(" What is the name of the segment?","Segment "+podcastNumber);
  document.getElementById("demo").innerHTML += "  <input type='text' name='desc"+podcastNumber+"' value='"+podcastName+"'/><br>";
  podcastNumber++;
}

