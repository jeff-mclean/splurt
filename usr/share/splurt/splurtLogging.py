#!/usr/bin/python3
import sys, os, time, requests, subprocess, datetime, pdb, psutil, splurtProgram, logging
from configobj import ConfigObj
from pathlib import Path

# this code is created to run every minute via a cron job on the splurt server
# it checks to see whether the sound file that SHOULD be recording this minute actually is,
# and if it isn't it kicks off the recording process in either high or low quality, depending
# upon whether it is being recorded for statutory logging purposes only, or whether it's a live
# program that we want to present as audio on demand.

#get configuration parameters
config = ConfigObj('/etc/splurt/splurt.conf')
workdir = config['workdir']
pcastwork = config['pcastwork']
cutPodcasts = config['cutPodcasts']
logwork = config['logwork']
logfileName = config['logfile']
lqdays = config['days']
hqdays = config['hqdays']
pfdays = config['pfdays']
soundLogRate = config['soundLogRate']
soundLogBits = config['soundLogBits']
soundLogChannels = config['soundLogChannels']
soundPodcastRate = config['soundPodcastRate']
soundPodcastBits = config['soundPodcastBits']
soundPodcastChannels = config['soundPodcastChannels']
recFilenameStore = config['recFilenameStore']
autotriggerAutocut = config['autotriggerAutocut']
generateAac = config['generateAac']

# create log file
logging.basicConfig(
        filename=logfileName,
        level=logging.DEBUG,
        format="%(asctime)s:%(levelname)s:%(message)s"
        )

#setup time variables
nowTime = datetime.datetime.now()
epoch = int(nowTime.strftime("%s"))
year = str(nowTime.year)                        # for year directory
time = nowTime.strftime("%Y-%m-%d-%H-%M-%S")    # for filenames
puredate = nowTime.strftime("%Y-%m-%d")         # for ogg tags
progtime = str(nowTime.weekday() + 1) + nowTime.strftime("%H%M") #`date +%u%0k%0M%0S` 	# for finding program names
dow = str(nowTime.weekday() + 1) #`date +%A`

# check log and podcast year subdirectories exist, and if not, create them
pcastPath = workdir + pcastwork + year
logPath = workdir + logwork + year
if not os.path.isdir(pcastPath) :
    logging.debug ("making " + pcastPath)
    os.makedirs (pcastPath)
if not os.path.isdir(logPath) :
    logging.debug ("making " + logPath)
    os.makedirs (logPath)

# get the current program (cP) name, and convert it for use in filenames
currentGrid = splurtProgram.Grid()
cP = currentGrid.getProgram(progtime)

progname = cP.showName.replace(" ","-")

# find out if we're currently recording this program
recording = False
for process in psutil.process_iter() :
    cmdline = process.cmdline()
    for portion in cmdline :
        if "rec" in portion and progname in portion :
            recording = True
            break
# if we're not recording this program, then we'd better start
if recording == False :

    # write some new stuff to the logs
    logging.info ( "************************************")
    logging.info ( "Logging script commencing           ")
    logging.info ( "************************************")
    logging.info ( "Deleting files...")
    logging.debug ("workdir = {}, pcastwork = {}, logwork = {}".format(workdir,pcastwork,logwork))
    # get rid of old files
    for r, d, f in os.walk(workdir):
        for item in f :
            fullpath = os.path.join(r, item)
            remove = False
            days = 0
            if fullpath.startswith(workdir + pcastwork) :
                remove = True
                days = hqdays
            if fullpath.startswith(workdir + logwork):
                remove = True
                days = lqdays
            if fullpath.startswith(workdir + cutPodcasts):
                remove = True
                days = pfdays
            deletionEpoch = epoch - (int(days) * 86400)
            logging.debug ("")
            logging.debug (fullpath)
            logging.debug (os.stat(fullpath))
            logging.debug ("epoch = {}, deletionEpoch = {}, remove = {}, days = {}".format(epoch,deletionEpoch,remove,days))
            if remove :
                logging.debug ("  deleting!!")
                logging.debug ("  stat = " + str(os.stat(fullpath).st_mtime))
                logging.debug ("  file = " + fullpath)
                logging.debug ("  needs to be days old = " + days)
                logging.debug ("  is actually days old = " + str((epoch - os.stat(fullpath).st_mtime)/86400))
                if os.stat(fullpath).st_mtime < deletionEpoch:
                    os.remove(fullpath)
                    logging.info ( "Deleted " + fullpath)
    logging.info ( "End of deleted files...")

    #start recording new file

    if cP.quality == "H" :
        soundRate = soundPodcastRate
        soundChannels = soundPodcastChannels
        soundBits = soundPodcastBits
        recDir = pcastwork
    else:
        soundRate = soundLogRate
        soundChannels = soundLogChannels
        soundBits = soundLogBits
        recDir = logwork

    recordingFilename = workdir + recDir + year + "/" + progname + "-" + time + ".ogg"
    logging.info ( "Recording file  " + recordingFilename + " for " + str(cP.remaining) + " seconds of quality type " + cP.quality)
    recFilenameStoreFile = open(workdir + recFilenameStore, "w")
    print ( recordingFilename, file=recFilenameStoreFile)
    recFilenameStoreFile.close()
    cmd = 'rec \
            -r {} \
            -c {} \
            --add-comment "artist={}" \
            --add-comment "comment={}" \
            --add-comment "date={}" \
            --add-comment "title={}" \
            -b {} \
            -q \
            "{}" \
            trim 0 {}'.format(soundRate, soundChannels, cP.presenterName, cP.showDesc, puredate,
                              cP.showName, soundBits, recordingFilename,
                              cP.remaining).strip()
    logging.debug (cmd)
    subprocess.call(cmd, shell=True)
    if generateAac == "True" :
        aacFileName = Path(recordingFilename).with_suffix('.aac')
        cmd = 'nice -n 19 ffmpeg \
                -i \"{}\" \
                -b:a 128k \
                \"{}\"'.format(recordingFilename,aacFileName)
        logging.debug (cmd)
        subprocess.call(cmd, shell=True)
    if autotriggerAutocut != 0 and cP.quality == "H" :
        # code for autotrigger
        requests.get("http://localhost/cgi-bin/cutlist.py?file=" + recordingFilename + "a")
        logging.info ( "Requested autotrigger on " + recordingFilename)
