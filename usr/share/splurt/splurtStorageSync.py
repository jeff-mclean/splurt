#!/usr/bin/python3
#import sys, os, subprocess, time
import subprocess, os, time, socket
from configobj import ConfigObj

# get command line parameters
config = ConfigObj('/etc/splurt/splurt.conf')
adminEmail = config['adminEmail']
syncLocationsConfig = config['syncLocations']
syncSucceed = config['syncSucceed']
syncFail = config['syncFail']
workdir = config['workdir']
subdirsConfig = config['subdirsToSync']
succeedFile = open(syncSucceed,"a")
failFile = open(syncFail,"a")

syncLocations = syncLocationsConfig.split(";")
subdirs = subdirsConfig.split(";")
for location in syncLocations :
    if location != "":
        strtime = time.asctime( time.localtime(time.time()) )
        if os.path.ismount(location)  :
            for subdir in subdirs :
                fromDir = workdir + subdir
                toDir = location + subdir
                cmd = "rsync  -avh  " + fromDir + " " + toDir
                result = subprocess.call(cmd, shell=True)
                if result == 0  :
                    print (succeedFile, strtime + ":" + fromDir, file=succeedFile)
                else :
                    print (strtime + ":" + fromDir, file=failFile)
        else :
            cmd = "echo \"\" | mutt -s \"" + location + " not mounted on " + \
                socket.gethostname() + "\" " + adminEmail
            print (cmd)
            result = subprocess.call(cmd, shell=True)
            print (location + " not mounted")
